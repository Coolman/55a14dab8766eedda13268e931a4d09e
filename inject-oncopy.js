// ==UserScript==
// @name         inject-oncopy
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Demand the COPY power
// @author       Coolman <QQ Group ID: 892850160>
// @match        https://segmentfault.com/*
// @match        https://www.yuque.com/*
// @match        https://android.myapp.com/myapp/detail.htm*
// @grant        none
// ==/UserScript==

(function() {
    // segmentfault
    document.oncopy = (t) => {
        t.preventDefault();
        let a = window.getSelection().toString();
        t.clipboardData.setData("text", a)
        console.log(a);
    };
    // yuque
    window.appData
        && window.appData.settings
        && window.appData.settings.disableDocumentCopy
        && (window.appData.settings.disableDocumentCopy = false);
    // https://android.myapp.com/myapp/detail.htm
    location.href.includes('https://android.myapp.com/myapp/detail.htm')
        && (document.head.insertAdjacentHTML('beforeEnd',
            `<style>::selection{background-color:#dcdcdc !important;}</style>`));
})();
